import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let {data,hanhdleViewDetail,handleAddItemToCart} = this.props;
    let {name, image, price} = data;
    return (

        <div className="col-3 my-3 "  >
          <div className="card text-left">
            <img className="card-img-top" src={image} alt = "Picture Shoe" />
            <div className="card-body">
              <h4 className="card-title" style={{height:50}}>{name}</h4>
              <p className="card-text">Price: {price}</p>
            </div>
            <button className="btn btn-info" onClick={() => { hanhdleViewDetail(data) }}>Detail Shoe</button>
            <button className="btn btn-success" onClick={() => { handleAddItemToCart(data) }}>Add Shoe</button>
          </div>
        </div>
    );
  }
}
