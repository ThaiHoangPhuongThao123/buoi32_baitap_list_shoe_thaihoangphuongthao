import React, { Component } from 'react'

export default class DetailShoe extends Component {
  render() {
    let {detailShoe} = this.props;
    let {name, image, price, description} = detailShoe;
    return (
     <div className='my-5 border border-info'>
        <h2  className='bg-info text-white text-center'>Details</h2>
         <div className='row'>
        <div className='col-4'>
            <h4 className='text-center text-info'>{name}</h4>
            <img src={image} alt="Picture Detail" />
        </div>
        <div className='col-8'>
           <table class="table table-striped table-inverse table-responsive">
            <thead class="thead-inverse">
                <tr>
                    <th colSpan={2} className='text-info'>Technical information</th>
                    
                </tr>
                </thead>
                <tbody>
                    <tr>
                <td> <strong className='text-info'>Name</strong> </td>
                <td>{name}</td>
            </tr>
            <tr>
                <td><strong className='text-info'>Price</strong> </td>
                <td>{price}</td>
            </tr>
            <tr>
                <td><strong className='text-info'>Description</strong> </td>
                <td>{description}</td>
            </tr>
                </tbody>
           </table>
        </div>
      </div>
     </div>
    )
  }
}
