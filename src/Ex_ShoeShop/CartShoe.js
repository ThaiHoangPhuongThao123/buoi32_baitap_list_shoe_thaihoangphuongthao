import React, { Component } from 'react'

export default class CartShoe extends Component {
  render() {
    let {cart,handleDeleteItem,hanhdleChangeAmountItem} = this.props;
    
    return (
      <div className='boder border-success mb-5 col-6'>
            <h2 className='bg-success text-white text-center'>Cart</h2>

        <table className="table">
            <thead>
                <tr>
                    <th className='text-success'>Name</th>
                    <th className='text-success'>Amount</th>
                    <th className='text-success'>Price</th>
                    <th className='text-success'> Image</th>
                    <th className='text-success'> Action</th>
                </tr>
            </thead>
            <tbody>
                {cart.map((item) => { 
                  return  <tr>
                    <td scope="row">{item.name}</td>
                    <td><button className='btn btn-warning mx-2' onClick={() => { hanhdleChangeAmountItem(item, -1) }}>-</button>
                        {item.amount}
                        <button className='btn btn-primary mx-2' onClick={() => { hanhdleChangeAmountItem(item, 1) }}>+</button></td>
                    <td>{item.price * item.amount}</td>
                    <td><img style={{width: 50}} src={item.image} alt="Picture item in Cart" /></td>
                    <td><button className='btn btn-danger' onClick={() => { handleDeleteItem(item.id) }}>X</button></td>

                </tr>
                 })}
            </tbody>
        </table>
      </div>
    )
  }
}
