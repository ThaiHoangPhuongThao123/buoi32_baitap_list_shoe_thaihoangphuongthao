import React, { Component } from 'react'
import { shoeArr } from './data'
import ListShoe from './ListShoe'
import DetailShoe from './DetailShoe'
import CartShoe from './CartShoe'

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr : shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  }
  hanhdleViewDetail = (shoe) => { 
    this.setState({
      detailShoe: shoe,
    })
   }
   handleAddItemToCart = (shoe) => { 
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => { 
      return item.id == shoe.id;
     })
      console.log('index: ', index);

     if( index == -1) {
      let newShoe = {...shoe, amount: 1};
    cloneCart.push(newShoe);
     } else {
    cloneCart[index].amount += 1;

     }
    this.setState({
      cart: cloneCart,
    })
    // th1: shoe chưa có trong cart => push
    // th2: shoe đã có trong cart => k push 
    }
    handleDeleteItem = (idShoe) => { 
      let cloneCart = [...this.state.cart];
      let index = cloneCart.findIndex((item) => { 
        return idShoe == item.id
       })
       if(index != -1) {
        cloneCart.splice(index, 1);
       }
       this.setState({
        cart: cloneCart,
       })
     }
     hanhdleChangeAmountItem = (shoe, option) => { 
      let cloneCart = [...this.state.cart];
      console.log('cloneCart: ', cloneCart);
      let index = cloneCart.findIndex((item) => { 
        return shoe.id == item.id;
       })
        

       if (index != -1) {
        cloneCart[index].amount =  cloneCart[index].amount +  option;
        if (cloneCart[index].amount < 1) {
          cloneCart.splice(index, 1)
        }
       }
       this.setState({
        cart: cloneCart,
       })
      }
  render() {
    return (
      <div>
      <div className='row'>
          <CartShoe cart = {this.state.cart}
        handleDeleteItem = {this.handleDeleteItem}
        hanhdleChangeAmountItem = {this.hanhdleChangeAmountItem}/>
        <ListShoe shoeArr = {this.state.shoeArr}
        hanhdleViewDetail = {this.hanhdleViewDetail}
        handleAddItemToCart = {this.handleAddItemToCart}/>
      </div>
        <DetailShoe detailShoe = {this.state.detailShoe}/>
      </div>
    )
  }
}
