import React, { Component } from 'react'
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
    renderListShoe = () => { 
    let {shoeArr} = this.props;
    return shoeArr.map((item, index) => { 
        return <ItemShoe key={index}
        data = {item}
        hanhdleViewDetail = {this.props.hanhdleViewDetail}
        handleAddItemToCart = {this.props.handleAddItemToCart}/>
     })
        
     }
  render() {
    return (
      <div className='col-6'><div className='row'>{this.renderListShoe()}</div></div>
      
    )
  }
}
